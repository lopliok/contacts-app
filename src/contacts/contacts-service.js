import axios from "axios"

export default {
    getContacts() {
        return axios.get('http://localhost:3001/contacts/')
    },

    searchContacts(query) {
        return axios.get('http://localhost:3001/contacts/?q=' + query)
    },

    createContact(data) {
        return axios.post('http://localhost:3001/contacts/', data)
    },

    getContact(id) {
        return axios.get('http://localhost:3001/contacts/' + id)
    },

    updateContact(contact) {
        return axios.put('http://localhost:3001/contacts/' + contact.id, contact)
    }


}