import React from 'react'

const ContactForm = ({ contact }) => {
    const handleChange = (e) => {
        contact[e.target.name] = e.target.value
    }

    return (
        <form>
            <div className="form-row">
                <div className="form-group container-fluid">
                    <label>Jméno</label>
                    <input type="text" className="form-control" name="name" value={contact.name} onChange={handleChange} />
                    
                </div>
            </div>
            <div className="form-group container-fluid">
                <label>Email</label>
                <input type="text" className="form-control" name="email" value={contact.email} onChange={handleChange} />
            </div>
            <div className="form-group container-fluid">
                <label>Tel.</label>
                <input type="text" className="form-control" name="phone" value={contact.phone} onChange={handleChange} />
            </div>
            <div className="form-group container-fluid">
                <label>Poznámka</label>
                <textarea className="form-control" name="note" value={contact.note} onChange={handleChange} rows="5">
                </textarea>
            </div>
        </form>

    )
}


export default ContactForm