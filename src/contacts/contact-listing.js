import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import _ from 'lodash'

import contactsService from './contacts-service'

class ContactListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: []
    }
  }
  search: null
  
  
    componentWillMount() {
      this.load()
    }
  
    async load() {
      
      const res = this.search ? await contactsService.searchContacts(this.search) : await contactsService.getContacts()
  
      this.setState({
        contacts: res.data
      })
  
    }
  
   
  
    handleChange = (e) => {
  
      this.search = e.target.value
      //_.debounce(this.load(), 200)
      this.load()
    }
  



  render() {
    const contacts = this.state.contacts

    return (
      <div>
        <div className="list-group-item list-group-item-action active">
          <div className="d-flex">
            <div className="ml-auto input-group">
              <input type="text" className="ml-auto form-control-sm" placeholder="Vyhledávání" name="search" onChange={this.handleChange} />
            </div>
          </div>

        </div>
        <div className="list-group">
          {contacts.map(c =>

            <Link to={ROUTES.getUrl(ROUTES.CONTACTS_DETAIL, { id: c.id })} key={c.id} className="list-group-item list-group-item-action">{c.name}</Link>
          )}
        </div>
        <Link to={ROUTES.CONTACTS_NEW} className="btn btn-light" >Create new</Link>
      </div>

    )

  }
}




export default ContactListing
