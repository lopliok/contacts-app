import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import contactsService from './contacts-service'

import Collapsible from '../collapsible'

class ContactDetail extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            contact: {}
        }
    }

    componentWillMount() {
        this.load(this.props.match.params.id)
    }

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id) {
            this.load(newProps.match.params.id)
        }
    }

    async load(id) {
        const res = await contactsService.getContact(id)


        this.setState({
            contact: res.data
        })
    }

    render() {
        const contact = this.state.contact

        return (
            <div>
                <h2>Detail</h2>
                <div className="btn-group">
                    <Link className="btn btn-light" to={ROUTES.getUrl(ROUTES.CONTACTS_EDITOR, { id: contact.id })}>Edit</Link>
                    <Link className="btn btn-danger" to={ROUTES.CONTACTS_LISTING}>Delete</Link>
                </div>

                <table>
                    <tbody>
                        <tr key={contact.id}>
                            <th>Name</th>
                            <td className="table-dark">{contact.name}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td className="table-primary">{contact.phone}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td className="table-secondary">{contact.email}</td>
                        </tr>
                        <tr>
                            <th>Note</th>
                            <td className="table-warning">
                                <Collapsible>
                                    {contact.note}
                                </Collapsible>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        )
    }
}




export default ContactDetail


