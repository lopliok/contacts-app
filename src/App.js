import React from 'react'

import { HashRouter, Route, NavLink, Switch, Link } from 'react-router-dom'

import Auth from './auth-service'

import Login from './login'
import ContactListing from './contacts/contact-listing'
import ContactEditor from './contacts/contact-editor'
import ContactDetail from './contacts/contact-detail'
import ContactNew from './contacts/contact-new'
import UserListing from './users/user-listing'
import UserEditor from './users/user-editor'
import UserNew from './users/user-new'

import ROUTES from './routes'




const App = () =>
  <HashRouter>
    <div>
      <Navbar />
      {(!Auth.isLoggedIn()) && <Login />}
      {(Auth.isLoggedIn()) && <div>
        <Switch>
          <Route path={ROUTES.USER_EDITOR} component={UserEditor} />
          <Route path={ROUTES.USER_LISTING} component={UserListing} />
          <Route path={ROUTES.USER_NEW} component={UserNew} />
          <Route path={ROUTES.CONTACTS_EDITOR} component={ContactEditor} />
          <Route path={ROUTES.CONTACTS_DETAIL} component={ContactDetail} />
          <Route path={ROUTES.CONTACTS_NEW} component={ContactNew} />
          <Route path={ROUTES.CONTACTS_LISTING} component={ContactListing} />
        </Switch>
      </div>}
    </div>
  </HashRouter>

const Navbar = () =>
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
      <li className="nav-item active">
        <Link to={ROUTES.CONTACTS_LISTING} className="navbar-brand">Adresář</Link>
      </li>
      <li className="nav-item">
        <NavLink exact to={ROUTES.USER_LISTING} className="nav-link">Uživatelé</NavLink>
      </li>
      <li className="nav-item">
        <NavLink exact to={ROUTES.CONTACTS_LISTING} className="nav-link" >Kontakty</NavLink>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn btn-outline-success my-2 my-sm-0 nav-link" onClick={Auth.logout}>Odhlásit</button>
    </form>
  </nav>





export default App;
