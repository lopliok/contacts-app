export default {
  CONTACTS_LISTING: '/',
  CONTACTS_NEW: '/new',  
  CONTACTS_DETAIL: '/detail/:id',
  CONTACTS_EDITOR: '/editor/:id',

  USER_LISTING: '/users',
  USER_EDITOR: '/users/:id',
  USER_NEW: '/newuser',

  getUrl(path, params) {
    return path.replace(/:(\w+)/g, (m, k) => params[k])
  }

}