import axios from 'axios'


export default {
    user: null,

    onChange: null,

    init() {
        this.reload()
    },

    async reload() {
        try {
            const res = await axios.get('http://localhost:3001/user')
            this.user = res.data
        } catch (e) {
            this.user = null
        }

        this.onChange()
    },

    isLoggedIn() {
        return this.user !== null
    },

    async login() {
        await axios.post('http://localhost:3001/login')
        this.reload()
    },

    async logout() {
        await axios.post('http://localhost:3001/logout')
        this.reload()
    }

}