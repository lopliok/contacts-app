import React from 'react'
import ROUTES from '../routes'

import usersService from './users-service'
import UserForm from './user-form'

class UserNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {
          name: "",
          login: ""
      }
    }
  }

 

  async create() {
    await usersService.createUser(this.state.user)
      
    this.props.history.push(ROUTES.USER_LISTING)
    

  }


  render() {
    const user = this.state.user

    return (
      <div className="container-fluid">
        <br />
        {user && <UserForm user={user} />}
        <button className="btn btn-primary" onClick={() => this.create()}>Uložit</button>
      </div>
    )

  }


}



export default UserNew
