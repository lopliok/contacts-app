import axios from "axios"

export default {
    getUsers() {
        return axios.get('http://localhost:3001/users/')
    },

    getUser(id) {
        return axios.get('http://localhost:3001/users/' + id)
    },

    updateUser(user) {
        return axios.put('http://localhost:3001/users/' + user.id, user)
    },
    
    createUser(data) {
        return axios.post('http://localhost:3001/users/',  data)
    }

}