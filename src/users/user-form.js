import React from 'react'

const UserForm = ({ user }) => {
    const handleChange = (e) => {
        user[e.target.name] = e.target.value
    }

    return (
        <form>
        <div className="form-group row">
          <label className="col-sm-1 col-form-label">Jméno</label>
          <div className="col-sm-5">
            <input type="text" readonly className="form-control" name="name" value={user.name} onChange={handleChange}  />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-1 col-form-label">login</label>
          <div className="col-sm-5">
            <input type="text" className="form-control" name="login" value={user.login} onChange={handleChange} />
          </div>
        </div>
      </form>
    )
}


export default UserForm