import React from 'react'
import Auth from './auth-service'

const Login = () =>
    <div className="container-fluid">

        <form className="form-signin">
            <h2 className="form-signin-heading">Please sign in</h2>
            <label className="sr-only">Email address</label>
            <input type="email" id="inputEmail" className="form-control" placeholder="Email address" />
            <label className="sr-only">Password</label>
            <input type="password" id="inputPassword" className="form-control" placeholder="Password" />
            <div className="checkbox">
                <label>
                    <input type="checkbox" value="remember-me" /> Remember me
          </label>
            </div>
            <button className="btn btn-lg btn-primary btn-block" onClick={Auth.login}>Sign in</button>
        </form>

    </div>

export default Login
