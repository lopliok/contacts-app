import React from 'react'

import cx from 'classnames'


class Collapsible extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            collapsed: true
        }
    }

    toogle() {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }




    render() {
        return (
            <div className={cx("collapsible", {collapsed: this.state.collapsed})}>
                <div className="content">
                    {this.props.children}
                </div>
                <button className="btn btn-link" onClick={() => this.toogle()}>
                    {
                        this.state.collapsed ? "Více ..." : "Zabalit"
                    }
                </button>
            </div>

        )
    }



}



export default Collapsible
