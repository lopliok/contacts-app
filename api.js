const express = require('express')
const cors = require('cors')
const jsonServer = require('json-server')

var delay = require('express-delay');



const app = express()

let user = null

app.use(cors())
app.use(delay(700));


app.get('/user', (req, res) => {
  res.send(user || 401)
})

app.post('/login', (req, res) => {
  user = {
    login: 'admin',
    username: 'Administrator'
  }
  res.end()
})

app.post('/logout', (req, res) => {
  user = null
  res.end()
})

const router = jsonServer.router('db.json')
app.use(jsonServer.bodyParser)
app.use(router)


app.listen(3001)
